package com.system.energymngtauth.service;

import com.system.energymngtusers.dto.PersonDTO;
import jakarta.servlet.http.HttpSession;

public interface AuthService {
    PersonDTO authenticatePerson(String username, String password, HttpSession session);

    void logoutPerson(HttpSession session);

    boolean isPersonLoggedIn(HttpSession session);
}
