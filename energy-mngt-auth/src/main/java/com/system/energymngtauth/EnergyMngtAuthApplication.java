package com.system.energymngtauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.system.energymngtauth", "com.system.energymngtusers"})
public class EnergyMngtAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnergyMngtAuthApplication.class, args);
	}

}
