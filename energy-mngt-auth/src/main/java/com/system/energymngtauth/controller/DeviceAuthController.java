package com.system.energymngtauth.controller;

import com.system.energymngtdevices.dto.DeviceDTO;
import com.system.energymngtdevices.dto.DevicePatchDTO;
import com.system.energymngtdevices.dto.NewDeviceDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
public class DeviceAuthController {
    @Autowired
    @Qualifier("restTemplateAuth")
    private final RestTemplate restTemplate;

    public DeviceAuthController(@Qualifier("restTemplateAuth") RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/auth/devices")
    @ResponseBody
    public List<DeviceDTO> getDevices(){
        DeviceDTO[] devices = restTemplate.exchange("http://localhost:8080/devices", HttpMethod.GET, null, DeviceDTO[].class).getBody();
        return List.of(devices);
    }

    @PostMapping("/auth/create-device")
    public ResponseEntity<?> createDevice(@RequestBody NewDeviceDTO newDeviceDTO) {
        HttpEntity<NewDeviceDTO> request = new HttpEntity<>(newDeviceDTO);
        return restTemplate.exchange("http://localhost:8080/create-device", HttpMethod.POST, request, DeviceDTO.class);
    }

    @PutMapping("/auth/update-device")
    public ResponseEntity<?> updateDevice(@RequestBody DevicePatchDTO devicePatchDTO) {
        HttpEntity<DevicePatchDTO> request = new HttpEntity<>(devicePatchDTO);
        return restTemplate.exchange("http://localhost:8080/update-device", HttpMethod.PUT, request, String.class);
    }

    @DeleteMapping("/auth/delete-device/{id}")
    public ResponseEntity<?> deleteDevice(@PathVariable Long id) {
        return restTemplate.exchange("http://localhost:8080/delete-device/{id}", HttpMethod.DELETE, null, String.class, id);
    }
}

