package com.system.energymngtauth.controller;

import com.system.energymngtconsumer.dto.MeasurementAggregateDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
public class MeasurementAuthController {
    @Autowired
    @Qualifier("restTemplateAuth")
    private final RestTemplate restTemplate;

    public MeasurementAuthController(@Qualifier("restTemplateAuth") RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/auth/measurements")
    @ResponseBody
    public List<MeasurementAggregateDTO> getMeasurementsForDeviceByDate(
            @RequestParam Long deviceId,
            @RequestParam String date,
            @RequestHeader("Authorization") String authToken) throws ParseException {

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", authToken);

        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<MeasurementAggregateDTO[]> response = restTemplate.exchange(
                "http://localhost:8082/measurements?deviceId=" + deviceId + "&date=" + date,
                HttpMethod.GET,
                entity,
                MeasurementAggregateDTO[].class);

        MeasurementAggregateDTO[] measurementAggregates = response.getBody();
        return List.of(measurementAggregates);
    }
}

