import { 
  addDeviceCards, 
  addUsersTable, 
  getCookie, 
  getUserRoleFromCookie, 
  getPersonIdFromCookie, 
  setupWSConnectionChat, 
  setupWSConnectionConsumer,
  sendMessage, 
  sendStatus, 
  loadChatHistory, 
  clearChatHistory,
  sendReceipt,
  getUsernameFromCookie
} from "./src/utils";
import { getDevices } from "./src/api/fetchDevices";
import { getUsers } from "./src/api/fetchUsers";
import { handleLogin, handleLogout } from "./src/api/handleAuthentication";
import { createAddButton as createDeviceButton } from "./src/components/createDevicesMenu";
import { createAddButton as createUserButton } from "./src/components/createUsersMenu";

export const toastrOptions = {
  closeButton: true,
  debug: false,
  newestOnTop: false,
  progressBar: true,
  positionClass: "toast-top-right",
  preventDuplicates: true,
  showDuration: "300",
  hideDuration: "1000",
  timeOut: "5000",
  extendedTimeOut: "1000",
  showEasing: "swing",
  hideEasing: "linear",
  showMethod: "fadeIn",
  hideMethod: "fadeOut",
};

// Navigate to a specific URL
function navigateTo(url) {
  history.pushState(null, null, url);
  renderContent(url);
}

// HTML templates
function getHomePageTemplate() {
  return `
    <div id="content" class="login-section">
      <div class="edit-card" style="width: 100%; max-width: 400px;">
        <div class="card-body">
          <header>
            <h2 class="device-title text-2xl" style="text-align: center;">ESM Application</h2>
          </header> 
          <div class="content" style="display: flex; flex-direction: column; align-items: center;">
            <form id="loginForm" style="width: 100%;">
              <div class="input-group" style="margin-bottom:1rem;">
                <label class="label-small-custom" for="username">Username</label>
                <input type="text" id="username" class="input-small-custom input-wide" placeholder="Enter your username" required>
              </div>
              <div class="input-group" style="margin-bottom: 2rem">
                <label class="label-small-custom" for="password">Password</label>
                <input type="password" id="password" class="input-small-custom input-wide" placeholder="Enter your password" required>
              </div>
              <div class="edit-options" style="width: 100%; display: flex; justify-content: center;">
                <button type="submit" class="save-button">Log In</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  `;
}

function getDevicesPageTemplate() {
  const userRole = getUserRoleFromCookie();

  return `
    <div id="content">
      <div class="devices-container">
        <div class="devices-section">
          ${
            userRole === "admin"
              ? `<div class="devices-menu">
                   <div class="devices-filter"></div>
                   <div class="devices-add"></div>
                 </div>`
              : ``
          }
          <div class="devices hidden"></div>
        </div>
        ${
          userRole === "admin"
            ? `<div class="edit-section">
                 <div class="edit-symbol">
                   <img src="src/assets/cpu-fill.svg" alt="Logo" class="edit-logo">
                 </div>
                 <div class="edit-holder"></div>
               </div>`
            : `<div class="chart-section">
                <div class="chart-symbol">
                  <img src="src/assets/bar-chart-line-fill.svg" alt="Logo" class="chart-logo">
                </div>
                <div class="chart-holder"></div>
              </div>`
        }
        </div>
    </div>
  `;
}

function getUsersPageTemplate() {
  return `
  <div id="content">
    <div class="users-container">
      <div class="users-section">
        <div class="users-menu">
          <div class="users-add">
          </div>
        </div>
        <div class="users hidden"></div>
      </div>
      <div class="edit-section">
          <div class="edit-symbol">
            <img src="src/assets/people-fill.svg" alt="Logo" class="edit-logo">
          </div>
        <div class="edit-holder"></div>
      </div>
    </div>
  </div>
  `;
}

function getChatPageTemplate() {
  return `
    <div id="content">
      <div class="chat-container">
        <div class="chat-section">
          <div class="chat-area">
            <div class="chat-messages-container"></div>
            <div id="typingIndicator" class="typing-indicator"></div>
            <div class="chat-input-container">
              <input type="text" id="chatInput" class="chat-input" placeholder="Type your message here..." />
              <button type="button" class="send-button">Send</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  `;
}

function setupNavigationEvents() {
  const navList = document.getElementById("nav-links");
  const loggedInUser = getCookie('userToken');
  const loggedInRole = getUserRoleFromCookie();
  
  if (loggedInUser) {
    const usersLink = document.createElement("li");
    usersLink.innerHTML = `<a href="/devices" class="text-white hover:text-black text-lg font-bold link-item">Devices</a>`;
    navList.appendChild(usersLink);
  }

  if (loggedInRole === 'admin') {
    const usersLink = document.createElement("li");
    usersLink.innerHTML = `<a href="/users" class="text-white hover:text-black text-lg font-bold link-item">Users</a>`;
    navList.appendChild(usersLink);
  }

  if (loggedInUser) {
    const chatLink = document.createElement("li");
    chatLink.innerHTML = `<a href="/chat" class="text-white hover:text-black text-lg font-bold link-item">Messages</a>`;
    navList.appendChild(chatLink);
  }

  const navLinks = document.querySelectorAll("nav a");
  navLinks.forEach((link) => {
    link.addEventListener("click", (event) => {
      event.preventDefault();
      const href = link.getAttribute("href");
      navigateTo(href);
    });
  });
}

function setupPopstateEvent() {
  window.addEventListener("popstate", () => {
    const currentUrl = window.location.pathname;
    renderContent(currentUrl);
  });
}

function setupInitialPage() {
  const initialUrl = window.location.pathname;
  renderContent(initialUrl);
}

function setupLoginForm() {
  const loginForm = document.getElementById('loginForm');
  if (loginForm) {
    loginForm.addEventListener('submit', handleLogin);
  }
}

function setupLogoutForm() {
  clearChatHistory();
  handleLogout();
}

function setupVisibilityChangeHandler(stompClient, personId, username) {
  document.addEventListener("visibilitychange", () => {
    if (document.visibilityState === 'visible') {
      const chatContainer = document.querySelector('.chat-container');
      if (chatContainer) {
        sendReceipt(stompClient, personId, username);
      }
    }
  });
}

function renderHomePage() {
  const loggedInUser = getCookie('userToken');

  if (loggedInUser) {
    window.location.href = '/devices';
    return;
  }
  
  const mainContentDiv = document.querySelector(".main-content-component");
  mainContentDiv.innerHTML = getHomePageTemplate();

  setupLoginForm();
}

function renderExitPage() {
  setupLogoutForm();
}

function renderDevicesPage() {
  const loggedIn = getCookie('userToken');
  const loggedInRole = getUserRoleFromCookie();

  if (!loggedIn) {
    window.location.href = '/';
    return;
  }

  const personId = getPersonIdFromCookie();
  if (personId) {
    setupWSConnectionConsumer(personId);
  }

  const mainContentDiv = document.querySelector(".main-content-component");
  mainContentDiv.innerHTML = getDevicesPageTemplate();

  if (loggedInRole === 'admin') {
    getDevices()
    .then((data) => {
      const devices = data;
      addDeviceCards(devices);
    })
    .finally(() => {
      setTimeout(() => {
        const listContainer = document.querySelector(".devices");
        listContainer.classList.remove("hidden");
      }, 500);
    });

    getUsers()
    .then((data) => {
      const users = data;
      // createDevicesFilterDropdown(users);
      createDeviceButton();
    })
    .finally(() => {
      setTimeout(() => {
        const menuContainer = document.querySelector(".devices-menu");
        menuContainer.classList.remove("hidden");
        const listContainer = document.querySelector(".devices-filter");
        listContainer.classList.remove("hidden");
      }, 500);
    });
  } else {
    const loggedInUser = getPersonIdFromCookie();

    getDevices(null, null, null, loggedInUser)
    .then((data) => {
      const devices = data;
      addDeviceCards(devices);
    })
    .finally(() => {
      setTimeout(() => {
        const listContainer = document.querySelector(".devices");
        listContainer.classList.remove("hidden");
      }, 500);
    });
  }
}

function renderUsersPage() {
  const loggedInRole = getUserRoleFromCookie();

  if (loggedInRole != 'admin') {
    navigateTo('/');
    return;
  }

  const mainContentDiv = document.querySelector(".main-content-component");
  mainContentDiv.innerHTML = getUsersPageTemplate();

  getUsers()
    .then((data) => {
      const users = data;
      addUsersTable(users);
      createUserButton();
    })
    .finally(() => {
      setTimeout(() => {
        const listContainer = document.querySelector(".users");
        listContainer.classList.remove("hidden");
      }, 500);
    });
}

function renderChatPage() {
  const loggedIn = getCookie('userToken');

  if (!loggedIn) {
    window.location.href = '/';
    return;
  }

  const mainContentDiv = document.querySelector(".main-content-component");
  mainContentDiv.innerHTML = getChatPageTemplate();

  // Get logged-in user's ID & username
  const personId = getPersonIdFromCookie();
  const username = getUsernameFromCookie();

  // Setup WebSocket connection for chat
  const stompClient = setupWSConnectionChat();

  if (personId) {
    setupWSConnectionConsumer(personId);
  }

  // Add event listener for send button
  document.querySelector('.send-button').addEventListener('click', () => {
    const messageContent = document.querySelector('#chatInput').value;
    if (messageContent && stompClient) {
      const messageId = `msg-${Date.now()}-${Math.random().toString(16).slice(2)}`;

      const message = { id: messageId, senderId: personId, senderUsername: username, content: messageContent };
      sendMessage(stompClient, message);
      document.querySelector('#chatInput').value = '';
    }
  });

  // Add typing detection logic
  let typingTimeout;
  const chatInput = document.querySelector('#chatInput');
  chatInput.addEventListener('input', () => {
    clearTimeout(typingTimeout);
    sendStatus(stompClient, personId, username, true);
    typingTimeout = setTimeout(() => {
      sendStatus(stompClient, personId, username, false);
    }, 2000);
  });

  // Setup read receipts mechanism
  setupVisibilityChangeHandler(stompClient, personId, username);

  // Load messages
  loadChatHistory();
}

// Render content based on URL
function renderContent(url) {
  const mainContentDiv = document.querySelector(".main-content-component");
  mainContentDiv.innerHTML = "";

  if (url === "/") {
    renderHomePage();
  } else if (url === "/devices") {
    renderDevicesPage();
  } else if (url === "/users") {
    renderUsersPage();
  } else if (url === "/chat") {
    renderChatPage();
  } else if (url === "/logout") {
    renderExitPage();
  }
}

// Call the setup functions
setupNavigationEvents();
setupPopstateEvent();
setupInitialPage();