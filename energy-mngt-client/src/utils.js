import { getDevice } from "./api/fetchDevices";
import { createDeviceCard } from "./components/createDeviceCard";
import { createUserRow, createUsersTable } from "./components/createUsersTable";
import toastr from "toastr";

export const addDeviceCards = (devices) => {
  const devicesContainer = document.querySelector(".devices");

  const mainMessage = document.createElement("h2");
  mainMessage.className = "main-message";
  mainMessage.innerText = "No devices available";

  devicesContainer.innerHTML = "";
  devicesContainer.appendChild(mainMessage);

  if (devices.length) {
    devicesContainer.innerHTML = "";

    for (let device of devices) {
      devicesContainer.appendChild(createDeviceCard(device));
    }
  }
};

export function refreshDeviceCard(device) {
  const deviceCardToUpdate = document.querySelector(
    `#device-card-${device.deviceId}`
  );
  const parentNode = deviceCardToUpdate.parentNode;

  getDevice(device.deviceId).then(() => {
    const updatedDeviceCard = createDeviceCard(device);
    parentNode.replaceChild(updatedDeviceCard, deviceCardToUpdate);
  });
}

export const addUsersTable = (users) => {
  const usersContainer = document.querySelector(".users");

  const mainMessage = document.createElement("h2");
  mainMessage.className = "main-message";
  mainMessage.innerText = "No users available";

  usersContainer.innerHTML = "";
  usersContainer.appendChild(mainMessage);

  if (users.length) {
    usersContainer.innerHTML = "";

    usersContainer.appendChild(createUsersTable(users));
  }
};

export function refreshUserRow(user) {
  const userRowToUpdate = document.getElementById(`user-row-${user.personId}`);
  if (userRowToUpdate) {
    const updatedUserRow = createUserRow(user);
    userRowToUpdate.parentNode.replaceChild(updatedUserRow, userRowToUpdate);
  } else {
    console.error(`No user row found for ID: ${user.personId}`);
  }
}

export function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(";").shift();
}

export function getUserRoleFromCookie() {
  const userToken = getCookie("userToken");

  if (!userToken) {
    return null;
  }

  const decodedToken = atob(userToken);

  const tokenParts = decodedToken.split(":");
  const role = tokenParts[1];

  return role;
}

export function getPersonIdFromCookie() {
  const userToken = getCookie("userToken");

  if (!userToken) {
    return null;
  }

  const decodedToken = atob(userToken);

  const tokenParts = decodedToken.split(":");
  const personId = tokenParts[0];

  return personId;
}

export function getUsernameFromCookie() {
  const userToken = getCookie("userToken");

  if (!userToken) {
    return null;
  }

  const decodedToken = atob(userToken);

  const tokenParts = decodedToken.split(":");
  const username = tokenParts[2];

  return username;
}

export function getAuthHeader() {
  const token = localStorage.getItem('jwtToken');
  return { 'Authorization': `${token}` };
}


/* Websocket component */ 
export function setupWSConnectionChat() {
  const socket = new SockJS("http://localhost:8083/ws-message");
  const stompClient = Stomp.over(socket);

  stompClient.connect(
    {},
    function (frame) {
      console.log("Connected: " + frame);

      // Subscribe to the chat topic
      stompClient.subscribe("/topic/messages", function (message) {
        onMessageReceived(JSON.parse(message.body), stompClient);
      });

      // Subscribe to typing status topic
      stompClient.subscribe('/topic/typing', function(message) {
        onStatusReceived(JSON.parse(message.body));
      });

      // Subscribe to read receipt topic
      stompClient.subscribe('/topic/receipts', function(receipt) {
        onReceiptReceived(JSON.parse(receipt.body));
      });
    },
    function (error) {
      console.error("WebSocket connection error:", error);
    }
  );

  return stompClient;
}

export function setupWSConnectionConsumer(personId) {
  const socket = new SockJS("http://localhost:8082/ws-message");
  const stompClient = Stomp.over(socket);

  stompClient.connect(
    {},
    function (frame) {
      console.log("Connected: " + frame);

      // Subscribe to the user's notification topic
      stompClient.subscribe(
        `/topic/user/${personId}/notifications`,
        function (notification) {
          console.log("DSDSDSDSDSDSDSD");
          toastr.error(notification.body, "Consumption Exceeded");
        }
      );
    },
    function (error) {
      console.error("WebSocket connection error:", error);
    }
  );

  return stompClient;
}


/* Messaging components */
export function sendMessage(stompClient, message) {
  if (message && stompClient && stompClient.connected) {
    stompClient.send("/app/chat", {}, JSON.stringify(message));
  } else {
    console.error("Unable to send message. WebSocket client not connected.");
  }
}

export function onMessageReceived(message, stompClient) {
  const currentUserId = getPersonIdFromCookie();
  const currentUsername = getUsernameFromCookie();

  const chatMessagesContainer = document.querySelector('.chat-messages-container');

  if(document.getElementById(`message-${message.id}`)) {
    return;
  }

  // Create a new message element
  const messageElement = document.createElement("div");
  messageElement.id = `message-${message.id}`;

  // Add 'sent' or 'received' class based on the senderId
  if (message.senderId === currentUserId) {
    messageElement.className = "chat-message sent";
  } else {
    messageElement.className = "chat-message received";
  }

  // Create a span for the message text
  const messageText = document.createElement("span");
  messageText.textContent = message.content;
  messageElement.appendChild(messageText);

  // Add the sender name
  if (message.senderId !== currentUserId) {
    const senderUsername = document.createElement("span");
    senderUsername.className = "sender-name";
    senderUsername.textContent = message.senderUsername;
    messageElement.appendChild(senderUsername);
  }

  // Append the new message to the chat
  chatMessagesContainer.appendChild(messageElement);

  // Scroll to the latest message
  scrollToBottom();

  // Send read receipt only if the message is from another user
  if (message.senderId !== currentUserId && isChatWindowActive()) {
    sendReceipt(stompClient, currentUserId, currentUsername);
  }

  // Save the message to localStorage
  saveChatHistory(message);
}


/* Typing components */
export function sendStatus(stompClient, id, username, typing) {
  if (stompClient && stompClient.connected) {
    const typingMessage = { senderId: id, senderUsername: username, typing: typing };
    stompClient.send("/app/typing", {}, JSON.stringify(typingMessage));
  }
}

export function onStatusReceived(message) {
  const typingIndicator = document.querySelector('#typingIndicator');
  const currentUserId = getPersonIdFromCookie();

  if (!typingIndicator) {
    console.error("Typing indicator element not found");
    return;
  }

  // Only display the typing status if the message is from another user
  if (message.senderId !== currentUserId) {
    if (message.typing) {
      typingIndicator.style.display = 'block';
      typingIndicator.textContent = `${message.senderUsername} is typing...`;
    } else {
      typingIndicator.style.display = 'none';
    }
  } else {
    typingIndicator.style.display = 'none';
  }
}


/* Saving to local storage components */
function saveChatHistory(message) {
  let messages;
  const storedMessages = localStorage.getItem('chatMessages');

  if (storedMessages) {
    try {
      messages = JSON.parse(storedMessages);
    } catch (e) {
      messages = [];
    }
  } else {
    messages = [];
  }

  const messageToSave = {
    id: message.id,
    senderId: message.senderId,
    senderUsername: message.senderUsername,
    content: message.content,
  };

  messages.push(messageToSave);
  localStorage.setItem('chatMessages', JSON.stringify(messages));
}

export function clearChatHistory() {
  localStorage.removeItem('chatMessages');
}

export function loadChatHistory() {
  const chatMessagesContainer = document.querySelector('.chat-messages-container');
  let messages;
  const storedMessages = localStorage.getItem('chatMessages');

  if (storedMessages) {
    try {
      messages = JSON.parse(storedMessages);
    } catch (e) {
      messages = [];
    }
  } else {
    messages = [];
  }

  const currentUserId = getPersonIdFromCookie();

  messages.forEach(message => {
    if(document.getElementById(`message-${message.id}`)) {
      return; 
    }

    const messageElement = document.createElement('div');
    messageElement.id = `message-${message.id}`;

    // Set class based on whether the message was sent or received
    if (message.senderId === currentUserId) {
      messageElement.className = 'chat-message sent';
    } else {
      messageElement.className = 'chat-message received';
    }

    // Create and append the message text
    const messageText = document.createElement('span');
    messageText.textContent = message.content;
    messageElement.appendChild(messageText);

    // Add the sender name for received messages
    if (message.senderId !== currentUserId) {
      const senderUsername = document.createElement('span');
      senderUsername.className = 'sender-name';
      senderUsername.textContent = message.senderUsername;
      messageElement.appendChild(senderUsername);
    }

    chatMessagesContainer.appendChild(messageElement);
  });

  // Scroll to the latest message
  scrollToBottom();
}


/* Read receipt components */
export function sendReceipt(stompClient, personId, username) {
  const messages = document.querySelectorAll('.chat-message');
  if (messages.length === 0) {
    return; 
  }

  if (stompClient && stompClient.connected) {
    const readReceipt = { readerId: personId, readerUsername: username };
    stompClient.send("/app/receipts", {}, JSON.stringify(readReceipt));
  }
}

export function onReceiptReceived(receipt) {
  const readerId = receipt.readerId;
  const readerUsername = receipt.readerUsername;
  const currentUserId = getPersonIdFromCookie();

  // Get all messages sent by the current user
  const myMessages = document.querySelectorAll('.chat-message.sent');

  myMessages.forEach((messageElement) => {
    let seenByElement = messageElement.querySelector('.seen-by-info');

    // Create the 'seen by' element if it doesn't exist
    if (!seenByElement) {
      seenByElement = document.createElement('div');
      seenByElement.className = 'seen-by-info';
      messageElement.appendChild(seenByElement);
    }

    // Update the 'seen by' list, making sure not to add the current user's ID
    const existingText = seenByElement.textContent;
    let newText;
    if (!existingText.includes(` ${readerUsername}`) && readerId !== currentUserId) {
      if (existingText) {
        newText = existingText + ", " + readerUsername;
      } else {
        newText = "Seen by " + readerUsername;
      }
      seenByElement.textContent = newText;
    }
  });

  scrollToBottom();
}

function isChatWindowActive() {
  const chatContainer = document.querySelector('.chat-container');
  return chatContainer && document.visibilityState === 'visible';
}

function scrollToBottom() {
  const chatMessagesContainer = document.querySelector('.chat-messages-container');
  chatMessagesContainer.scrollTop = chatMessagesContainer.scrollHeight;
}



