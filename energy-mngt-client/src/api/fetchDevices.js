import { API_DEVICES_URL } from "../config";
import { getAuthHeader } from "../utils";

export async function getDevice(id) {
  const response = await fetch(`${API_DEVICES_URL}/devices?deviceId=${id}`, {
    headers: getAuthHeader(),
  });
  const data = await response.json();
  return data;
}

export async function getDevices(deviceId = null, address = null, consumption = null, personId = null) {
  let url = `${API_DEVICES_URL}/devices`;

  // Build the query string
  const params = new URLSearchParams();
  if (deviceId) {
    params.append("deviceId", deviceId);
  }
  if (address && address !== "") {
    params.append("address", address);
  }
  if (consumption && consumption !== "") {
    params.append("consumption", consumption);
  }
  if (personId && personId !== "") {
    params.append("personId", personId);
  }

  if (params.toString()) {
    url += `?${params.toString()}`;
  }

  const response = await fetch(url, {
    headers: getAuthHeader(),
  });
  const data = await response.json();
  return data;
}