import { API_USERS_URL } from "../config";
import { getAuthHeader } from "../utils";

export async function getUser(id) {
  const response = await fetch(`${API_USERS_URL}/persons?personId=${id}`, {
    headers: getAuthHeader(),
  });
  const data = await response.json();
  return data;
}

export async function getUsers(personId = null, username = null, role = null) {
  let url = `${API_USERS_URL}/persons`;

  // Build the query string
  const params = new URLSearchParams();
  if (personId) {
    params.append("personId", personId);
  }
  if (username && username !== "") {
    params.append("username", username);
  }
  if (role && role !== "") {
    params.append("role", role);
  }

  if (params.toString()) {
    url += `?${params.toString()}`;
  }

  const response = await fetch(url, {
    headers: getAuthHeader(),
  });
  const data = await response.json();
  return data;
}