import { API_USERS_URL } from "../config";
import { getAuthHeader } from "../utils";

export function deleteUser(id) {
  return fetch(`${API_USERS_URL}/delete-person/${id}`, {
    method: "DELETE",
    headers: getAuthHeader(),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Failed to delete person");
      }
    })
    .catch((error) => {
      console.error("Error deleting the person:", error);
      throw error;
    });
}