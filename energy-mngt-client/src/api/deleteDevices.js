import { API_DEVICES_URL } from "../config";
import { getAuthHeader } from "../utils";

export function deleteDevice(id) {
  return fetch(`${API_DEVICES_URL}/delete-device/${id}`, {
    method: "DELETE",
    headers: getAuthHeader(),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Failed to delete device");
      }
    })
    .catch((error) => {
      console.error("Error deleting the device:", error);
      throw error;
    });
}
