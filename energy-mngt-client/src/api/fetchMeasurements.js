import { API_MEASUREMENTS_URL } from "../config";
import { getAuthHeader } from "../utils";

export async function getMeasurements(deviceId, date) {
  const formattedDate = new Date(date).toISOString().split('T')[0];
  
  let url = `${API_MEASUREMENTS_URL}/measurements?deviceId=${deviceId}&date=${formattedDate}`;

  const response = await fetch(url, {
    headers: getAuthHeader(),
  });
  
  if (!response.ok) {
    throw new Error(`HTTP error! status: ${response.status}`);
  }
  const data = await response.json();
  return data;
}
