import { API_DEVICES_URL } from "../config";
import { getAuthHeader } from "../utils";

export function createDevice(name, description, address, consumption, personId) {
  return fetch(`${API_DEVICES_URL}/create-device`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      ...getAuthHeader(),
    },
    body: JSON.stringify({
      name: name,
      description: description,
      address: address,
      consumption: consumption,
      personId: personId
    }),
  }).then((response) => {
    if (!response.ok) {
      throw new Error("Failed to create device.");
    }
  })
  .catch((error) => {
    console.error("Error creating the device:", error);
    throw error;
  });
}
