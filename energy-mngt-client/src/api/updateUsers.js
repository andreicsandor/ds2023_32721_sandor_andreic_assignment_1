import { API_USERS_URL } from "../config";
import { getAuthHeader } from "../utils";

export function updateUser(id, username, password, role) {
  return fetch(`${API_USERS_URL}/update-person`, {
    // method: "PATCH", 
    method: "PUT", 
    headers: {
      "Content-Type": "application/json",
      ...getAuthHeader(),
    },
    body: JSON.stringify({
      id: id,
      username: username,
      password: password,
      role: role
    }),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Failed to update person");
      }
      return response.json();
    })
    .catch((error) => {
      console.error("Error updating the person:", error);
      throw error;
    });
}