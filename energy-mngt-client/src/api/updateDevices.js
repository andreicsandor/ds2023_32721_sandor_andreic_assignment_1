import { API_DEVICES_URL } from "../config";
import { getAuthHeader } from "../utils";

export function updateDevice(id, name, description, address, consumption, personId) {
  return fetch(`${API_DEVICES_URL}/update-device`, {
    // method: "PATCH", 
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      ...getAuthHeader(),
    },
    body: JSON.stringify({
      id: id,
      name: name,
      description: description,
      address: address,
      consumption: consumption,
      personId: personId
    }),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Failed to update device");
      }
      return response.json();
    })
    .catch((error) => {
      console.error("Error updating the device:", error);
      throw error;
    });
}
