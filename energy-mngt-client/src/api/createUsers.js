import { API_USERS_URL } from "../config";
import { getAuthHeader } from "../utils";

export function createUser(username, password, role) {
  return fetch(`${API_USERS_URL}/create-person`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      ...getAuthHeader(),
    },
    body: JSON.stringify({
      username: username,
      password: password,
      role: role
    }),
  }).then((response) => {
    if (!response.ok) {
      throw new Error("Failed to create person.");
    }
  })
  .catch((error) => {
    console.error("Error creating the person:", error);
    throw error;
  });
}