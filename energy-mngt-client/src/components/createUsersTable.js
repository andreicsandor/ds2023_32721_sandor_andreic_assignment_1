import { deleteUser } from "../api/deleteUsers";
import { createEditUserCard } from "./createEditUserCard";

export function createUsersTable(users) {
  const table = document.createElement("table");
  table.className = "users-table";
  
  const thead = document.createElement("thead");
  thead.innerHTML = `
    <tr>
      <th>ID</th>
      <th>Username</th>
      <th>Password</th>
      <th>Role</th>
      <th></th>
    </tr>
  `;
  table.appendChild(thead);

  const tbody = document.createElement("tbody");
  users.forEach((user) => {
    const userRow = createUserRow(user);
    tbody.appendChild(userRow);
  });

  table.appendChild(tbody);

  return table;
}

export function createUserRow(user) {
  const tr = document.createElement("tr");
  tr.id = `user-row-${user.personId}`;
  tr.innerHTML = `
    <td>${user.personId}</td>
    <td>${user.username}</td>
    <td><span class="password-bullets">••••••••</span></td>
    <td>${user.role}</td>
    <td>
      <div class="button-container">
        <button class="update-button">
          <img src="./src/assets/pencil-fill.svg" alt="Edit">
        </button>
        <button class="delete-button">
          <img src="./src/assets/trash-fill.svg" alt="Delete">
        </button>
      </div>
    </td>
  `;
  attachEvents(user, tr);
  return tr;
}

async function deleteHandler(user, tr) {
  const userId = user.personId;

  const listContainer = document.querySelector(".users");
  listContainer.classList.add("hidden");

  deleteUser(userId)
    .then(() => {
      console.log("Person successfully deleted.")
      tr.remove();
    })
    .catch((error) => {
      console.error("Error deleting the person:", error);
    })

    .finally(() => {
      setTimeout(() => {
        const listContainer = document.querySelector(".users");
        listContainer.classList.remove("hidden");
        const editHolder = document.querySelector(".edit-holder");
        editHolder.classList.add("hidden");
        const symbolHolder = document.querySelector(".edit-symbol");
        symbolHolder.classList.remove("hidden");
      }, 500);
    });
}

function editHandler(user) {
  const symbolHolder = document.querySelector(".edit-symbol");
  symbolHolder.classList.add("hidden");
  const editHolder = document.querySelector(".edit-holder");
  const editableCard = createEditUserCard(user);
  editHolder.innerHTML = "";
  editHolder.appendChild(editableCard);
  editHolder.classList.remove("hidden"); 
}

function attachEvents(user, tr) {
  const deleteButton = tr.querySelector(".delete-button");
  deleteButton.addEventListener("click", function () {
    deleteHandler(user, tr);
  });

  const updateButton = tr.querySelector(".update-button");
  updateButton.addEventListener("click", function () {
    editHandler(user);
  });
}
