import { createDevice } from "../api/createDevices";

export function createNewDeviceCard() {
    const editableCard = document.createElement("div");
  
    const contentMarkup = `
      <div class="edit-card">
        <div class="card-body">
          <header>
            <div class="input-group">
              <div class="label-container">
                <label class="label-small-custom" for="name">Device name</label>
              </div>
              <input id="name" type="text" class="device-title text-2xl font-bold input-large-custom" placeholder="Enter name">
            </div>
          </header> 
          <div class="content" style="margin-bottom: 1.25rem;">
            <div class="input-group">
              <div class="label-container">
                <label class="label-small-custom" for="description">Device description</label>
              </div>
              <textarea id="description" class="description text-gray-700 input-small-custom input-wide" placeholder="Enter description"></textarea>
            </div>
            <div class="input-group">
              <div class="label-container">
                <label class="label-small-custom" for="address">Address of consumption</label>
              </div>
              <input id="address" type="text" class="text-gray-700 input-small-custom input-wide" placeholder="Enter address"/>
            </div>
            <div class="input-group">
              <div class="label-container">
                <label class="label-small-custom" for="consumption">Maximum hourly consumption (kWh)</label>
              </div>
              <input id="consumption" type="number" class="text-gray-700 input-small-custom input-large" style="font-size: 0.9rem;" placeholder="Enter consumption"/>
            </div>
            <div class="input-group">
              <div class="label-container">
                <label class="label-small-custom" for="user">User ID</label>
              </div>
              <input id="user" type="number" class="text-gray-700 input-small-custom input-wide" style="font-size: 0.9rem;" placeholder="Enter user ID"/>
            </div>
          </div>
          <div class="edit-options text-gray-700">
            <button class="save-button">
              Save
            </button>
          </div>
        </div>
      </div>
    `;
  
    editableCard.innerHTML = contentMarkup;

    attachEvents(editableCard);
  
    return editableCard;
  }

  function createHandler() {
    const newDeviceName = document.getElementById("name").value;
    const newDeviceDescription = document.getElementById("description").value;
    const newDeviceAddress = document.getElementById("address").value;
    const newDeviceConsumption = parseFloat(document.getElementById("consumption").value);
    const newUser = parseFloat(document.getElementById("user").value);
  
    if (!newDeviceName || !newDeviceDescription || !newDeviceAddress || isNaN(newDeviceConsumption || isNaN(newUser))) {
      console.error("Invalid input data.");
      return;
    }
  
    createDevice(newDeviceName, newDeviceDescription, newDeviceAddress, newDeviceConsumption, newUser)
      .then(() => {
        window.location.reload();
      })
      .catch((error) => {
        console.error("Error creating the device:", error);
      })
      .finally(() => {});
  }
  
  function attachEvents(editableCard) {
    const saveButton = editableCard.querySelector(".save-button");
  
    saveButton.addEventListener("click", function () {
        createHandler();
    });
  }