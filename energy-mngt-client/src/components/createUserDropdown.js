export function createUserDropdownItem(users) {
  if (!users || users.length === 0) return;

  const dropdown = document.createElement("select");
  dropdown.className = "filter-dropdown";
  dropdown.id = "userDropdown";

  const defaultOption = document.createElement("option");
  dropdown.appendChild(defaultOption);

  users.forEach((user) => {
    const option = document.createElement("option");
    option.value = user.personId;
    option.innerText = user.username;
    dropdown.appendChild(option);
  });

  dropdown.value = 0;

  return dropdown;
}
