import { createNewUserCard } from "./createNewUserCard";

export function createAddButton() {
    const devicesAddContainer = document.querySelector(".users-add");
  
    const buttonCreate = document.createElement("button");
    buttonCreate.innerText = "Add User";
    buttonCreate.className = "create-button";
  
    devicesAddContainer.appendChild(buttonCreate);

    attachEvents();
  }

function createHandler() {
  const symbolHolder = document.querySelector(".edit-symbol");
  symbolHolder.classList.add("hidden");
  
  const editHolder = document.querySelector(".edit-holder");
  const editableCard = createNewUserCard();
  editHolder.innerHTML = "";
  editHolder.appendChild(editableCard);
  editHolder.classList.remove("hidden"); 
}

function attachEvents() {
  const createButton = document.querySelector(".create-button");
  createButton.addEventListener("click", function () {
    createHandler();
  });
}
  