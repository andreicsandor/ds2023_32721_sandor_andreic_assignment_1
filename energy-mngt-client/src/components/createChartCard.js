import Chart from "chart.js/auto";
import { getMeasurements } from "../api/fetchMeasurements";

export function createChartCard(device) {
  const chartCard = document.createElement("div");
  chartCard.classList.add("chart-card");

  const today = new Date().toISOString().split("T")[0];

  const contentMarkup = `
    <div class="chart-header">
      <h2>${device.name}</h2>
      <input type="date" id="chartDate-${device.deviceId}" class="chart-date-input" value="${today}">
    </div>
    <div class="chart-container">
      <canvas id="consumptionChart-${device.deviceId}"></canvas>
    </div>
  `;

  chartCard.innerHTML = contentMarkup;

  const chartDateInput = chartCard.querySelector(
    `#chartDate-${device.deviceId}`
  );
  const canvas = chartCard.querySelector(
    `#consumptionChart-${device.deviceId}`
  );

  renderChartForDate(canvas, device.deviceId, today);

  chartDateInput.addEventListener("change", () => {
    const selectedDate = chartDateInput.value;
    renderChartForDate(canvas, device.deviceId, selectedDate);
  });

  return chartCard;
}

function renderChartForDate(canvas, deviceId, date) {
  if (canvas._chartInstance) {
    canvas._chartInstance.destroy();
  }

  if (date) {
    getMeasurements(deviceId, date)
      .then((data) => {
        const chartData = prepareChartData(data);
        canvas._chartInstance = renderChart(canvas, chartData);
      })
      .catch((error) => {
        console.error("Error fetching consumption data:", error);
      });
  } else {
    alert("Please select a date to view the consumption chart.");
  }
}

function prepareChartData(rawData) {
  const hourlyData = new Array(24).fill(0);
  rawData.forEach((point) => {
    hourlyData[point.hour] = point.hourlyConsumption;
  });

  const labels = hourlyData.map((_, index) => `${index}:00`);
  return {
    labels: labels,
    datasets: [
      {
        label: "Consumption (kWh)",
        data: hourlyData,
        fill: false,
        borderColor: "rgb(75, 192, 192)",
        tension: 0.1,
      },
    ],
  };
}

function renderChart(canvas, chartData) {
  canvas.style.width = "25rem";
  const ctx = canvas.getContext("2d");
  const chart = new Chart(ctx, {
    type: "bar",
    data: {
      labels: chartData.labels,
      datasets: [
        {
          label: "Consumption (kWh)",
          data: chartData.datasets[0].data,
          fill: false,
          backgroundColor: "#a7bcc9",
          borderWidth: 1,
        },
      ],
    },
    options: {
      maintainAspectRatio: false,
      scales: {
        y: {
          display: false,
          beginAtZero: true,
        },
      },
      responsive: true,
      plugins: {
        legend: {
          display: false,
          position: "top",
        },
        title: {
          display: true,
          text: "Hourly Consumption",
        },
      },
    },
  });

  canvas._chartInstance = chart;
  return chart;
}
