import { deleteDevice } from "../api/deleteDevices";
import { getDevice } from "../api/fetchDevices";
import { getUserRoleFromCookie } from "../utils";
import { createEditDeviceCard } from "./createEditDeviceCard";
import { createChartCard } from "./createChartCard";

export function createDeviceCard(device) {
  const userRole = getUserRoleFromCookie();
  const deviceCard = document.createElement("div");

  const contentMarkup = `
    <div class="device-card" id="device-card-${device.deviceId}">
      <div class="card-body">
        <header>
          <h2 class="device-title text-2xl font-bold">${device.name}</h2>
        </header> 
        <div class="content">
          <p class="description text-gray-700">${device.description}</p>
          <p class="date text-gray-700" style="font-size: 0.9rem;">${device.address}</p>
          <p class="date text-gray-700" style="font-size: 0.9rem;">${device.consumption} kWh</p>  
        </div>
        <div class="edit-options text-gray-700">
        ${
          userRole === 'admin'
            ? `<button class="update-button">
                  <img src="./src/assets/pencil-fill.svg" alt="Edit">
                </button>
                <button class="delete-button">
                  <img src="./src/assets/trash-fill.svg" alt="Delete">
                </button>`
            : `<button class="chart-button">
                  <img src="./src/assets/chart-fill.svg" alt="View">
                </button>`
          }
          </div>
      </div>
    </div>
  `;

  deviceCard.innerHTML = contentMarkup;

  attachEvents(device, deviceCard);

  return deviceCard;
}

async function deleteHandler(device, deviceCard) {
  const deviceId = device.deviceId;

  const listContainer = document.querySelector(".devices");
  listContainer.classList.add("hidden");

  deleteDevice(deviceId)
    .then(() => {
      console.log("Device successfully deleted.")
      deviceCard.remove();
    })
    .catch((error) => {
      console.error("Error deleting the device:", error);
    })

    .finally(() => {
      setTimeout(() => {
        const listContainer = document.querySelector(".devices");
        listContainer.classList.remove("hidden");
        const editHolder = document.querySelector(".edit-holder");
        editHolder.classList.add("hidden");
        const symbolHolder = document.querySelector(".edit-symbol");
        symbolHolder.classList.remove("hidden");
      }, 500);
    });
}

function editHandler(device) {
  const symbolHolder = document.querySelector(".edit-symbol");
  symbolHolder.classList.add("hidden");
  
  const editHolder = document.querySelector(".edit-holder");
  const editableCard = createEditDeviceCard(device);
  editHolder.innerHTML = "";
  editHolder.appendChild(editableCard);
  editHolder.classList.remove("hidden"); 
}

function chartHandler(device) {
  const symbolHolder = document.querySelector(".chart-symbol");
  symbolHolder.classList.add("hidden");
  
  const viewHolder = document.querySelector(".chart-holder");
  const chartCard = createChartCard(device);
  viewHolder.innerHTML = "";
  viewHolder.appendChild(chartCard);
  viewHolder.classList.remove("hidden"); 
}

function attachEvents(device, deviceCard) {
  const userRole = getUserRoleFromCookie();

  if (userRole === 'admin') {
    const deleteButton = deviceCard.querySelector(".delete-button");
    deleteButton.addEventListener("click", function () {
      deleteHandler(device, deviceCard);
    });

    const updateButton = deviceCard.querySelector(".update-button");
    updateButton.addEventListener("click", function () {
      editHandler(device);
    });
  } else {
    const chartButton = deviceCard.querySelector(".chart-button");
    chartButton.addEventListener("click", function () {
      chartHandler(device);
    });
  }
}
