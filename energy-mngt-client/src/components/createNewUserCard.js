import { createUser } from "../api/createUsers";

export function createNewUserCard() {
    const editableCard = document.createElement("div");
  
    const contentMarkup = `
      <div class="edit-card">
        <div class="card-body">
          <header>
            <div class="input-group">
              <div class="label-container">
                <label class="label-small-custom" for="password">Username</label>
              </div>
              <input id="username" type="text" class="user-title text-2xl font-bold input-large-custom" placeholder="Enter username">
            </div>
          </header> 
          <div class="content" style="margin-bottom: 1.25rem;">
            <div class="input-group">
              <div class="label-container">
                <label class="label-small-custom" for="password">Password</label>
              </div>
              <input id="password" type="password" class="text-gray-700 input-small-custom input-wide" placeholder="Enter password"/>
            </div>
            <div class="input-group">
              <div class="label-container">
                <label class="label-small-custom" for="role">Role</label>
              </div>
              <select id="role" class="text-gray-700 input-small-custom input-wide" style="font-size: 0.9rem;">
                <option value="admin">Administrator</option>
                <option selected value="client">Client</option>
              </select>          
            </div>
          </div>
          <div class="edit-options text-gray-700">
            <button class="save-button">
              Save
            </button>
          </div>
        </div>
      </div>
    `;
  
    editableCard.innerHTML = contentMarkup;

    attachEvents(editableCard);
  
    return editableCard;
  }

  function createHandler() {
    const newUsername = document.getElementById("username").value;
    const newPassword = document.getElementById("password").value;
    const newRole = document.getElementById("role").value;
  
    if (!newUsername || !newPassword || !newRole) {
      console.error("Invalid input data.");
      return;
    }
  
    createUser(newUsername, newPassword, newRole)
      .then(() => {
        window.location.reload();
      })
      .catch((error) => {
        console.error("Error creating the person:", error);
      })
      .finally(() => {});
  }
  
  function attachEvents(editableCard) {
    const saveButton = editableCard.querySelector(".save-button");
  
    saveButton.addEventListener("click", function () {
        createHandler();
    });
  }
  