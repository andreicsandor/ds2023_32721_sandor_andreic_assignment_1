import { updateDevice } from "../api/updateDevices";
import { refreshDeviceCard } from "../utils";

export function createEditDeviceCard(device) {
  const editableCard = document.createElement("div");

  const contentMarkup = `
    <div class="edit-card" id="edit-card-${device.deviceId}">
      <div class="card-body">
        <header>
          <div class="input-group">
            <div class="label-container">
              <label class="label-small-custom" for="name">Device name</label>
            </div>
            <input id="name" type="text" class="device-title text-2xl font-bold input-large-custom" placeholder="Enter name" value="${device.name}">
          </div>
        </header> 
        <div class="content" style="margin-bottom: 1.25rem;">
          <div class="input-group">
            <div class="label-container">
              <label class="label-small-custom" for="description">Device description</label>
            </div>
            <textarea id="description" class="description text-gray-700 input-small-custom input-wide" placeholder="Enter description">${device.description}</textarea>
          </div>
          <div class="input-group">
            <div class="label-container">
              <label class="label-small-custom" for="address">Address of consumption</label>
            </div>
            <input id="address" type="text" class="text-gray-700 input-small-custom input-wide" placeholder="Enter address" value="${device.address}"/>
          </div>
          <div class="input-group">
            <div class="label-container">
              <label class="label-small-custom" for="consumption">Maximum hourly consumption (kWh)</label>
            </div>
            <input id="consumption" type="number" class="text-gray-700 input-small-custom input-narrow" style="font-size: 0.9rem;" placeholder="Enter consumption" value="${device.consumption}"/>
          </div>
          <div class="input-group">
            <div class="label-container">
              <label class="label-small-custom" for="user">User ID</label>
            </div>
            <input id="user" type="number" class="text-gray-700 input-small-custom input-narrow" style="font-size: 0.9rem;" placeholder="Enter user ID" value="${device.personId}"/>
          </div>
        </div>
        <div class="edit-options text-gray-700">
          <button class="save-button">
            Save
          </button>
        </div>
      </div>
    </div>
  `;

  editableCard.innerHTML = contentMarkup;

  attachEvents(editableCard, device);

  return editableCard;
}

function updateHandler(device) {
  const deviceId = device.deviceId;
  
  const newDeviceName = document.getElementById("name").value;
  const newDeviceDescription = document.getElementById("description").value;
  const newDeviceAddress = document.getElementById("address").value;
  const newDeviceConsumption = parseFloat(document.getElementById("consumption").value);
  const newUser = parseFloat(document.getElementById("user").value);

  if (!newDeviceName || !newDeviceDescription || !newDeviceAddress || isNaN(newDeviceConsumption) || isNaN(newUser)) {
    console.error("Invalid input data.");
    return;
  }

  device.name = newDeviceName;
  device.description = newDeviceDescription;
  device.address = newDeviceAddress;
  device.consumption = newDeviceConsumption;
  device.personId = newUser;

  updateDevice(deviceId, newDeviceName, newDeviceDescription, newDeviceAddress, newDeviceConsumption, newUser)
    .then(() => {
      refreshDeviceCard(device);
    })
    .catch((error) => {
      console.error("Error updating the device:", error);
    })
    .finally(() => {
      setTimeout(() => {
        const editHolder = document.querySelector(".edit-holder");
        editHolder.classList.add("hidden");
        const symbolHolder = document.querySelector(".edit-symbol");
        symbolHolder.classList.remove("hidden");
      }, 500);
    });
}

function attachEvents(editableCard, device) {
  const saveButton = editableCard.querySelector(".save-button");

  saveButton.addEventListener("click", function () {
    updateHandler(device);
  });
}