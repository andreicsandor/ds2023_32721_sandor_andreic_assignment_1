import { updateUser } from "../api/updateUsers";
import { refreshUserRow } from "../utils";

export function createEditUserCard(user) {
  const editableCard = document.createElement("div");

  const contentMarkup = `
    <div class="edit-card" id="edit-card-${user.personId}">
      <div class="card-body">
        <header>
          <div class="input-group">
            <div class="label-container">
              <label class="label-small-custom" for="password">Username</label>
            </div>
            <input id="username" type="text" class="user-title text-2xl font-bold input-large-custom" placeholder="Enter username" value="${user.username}">
          </div>
        </header> 
        <div class="content" style="margin-bottom: 1.25rem;">
          <div class="input-group">
            <div class="label-container">
              <label class="label-small-custom" for="password">Password</label>
            </div>
            <input id="password" type="password" class="text-gray-700 input-small-custom input-wide" placeholder="Enter password" value="${user.password}"/>
          </div>
          <div class="input-group">
            <div class="label-container">
              <label class="label-small-custom" for="role">Role</label>
            </div>
            <select id="role" class="text-gray-700 input-small-custom input-wide" style="font-size: 0.9rem;">
              <option value="admin" ${user.role === 'admin' ? 'selected' : ''}>Administrator</option>
              <option value="client" ${!user.role || user.role === 'client' ? 'selected' : ''}>Client</option>
            </select>          
          </div>
        </div>
        <div class="edit-options text-gray-700">
          <button class="save-button">
            Save
          </button>
        </div>
      </div>
    </div>
  `;

  editableCard.innerHTML = contentMarkup;

  attachEvents(editableCard, user);

  return editableCard;
}

function updateHandler(user) {
  const personId = user.personId;

  const newUsername = document.getElementById("username").value;
  const newPassword = document.getElementById("password").value;
  const newRole = document.getElementById("role").value;

  if (!newUsername || !newPassword || !newRole) {
    console.error("Invalid input data.");
    return;
  }

  user.username = newUsername;
  user.password = newPassword;
  user.role = newRole;

  updateUser(personId, newUsername, newPassword, newRole)
    .then(() => {
      refreshUserRow(user);
    })
    .catch((error) => {
      console.error("Error updating the person:", error);
    })
    .finally(() => {
      setTimeout(() => {
        const editHolder = document.querySelector(".edit-holder");
        editHolder.classList.add("hidden");
        const symbolHolder = document.querySelector(".edit-symbol");
        symbolHolder.classList.remove("hidden");
      }, 500);
    });
}

function attachEvents(editableCard, user) {
  const saveButton = editableCard.querySelector(".save-button");

  saveButton.addEventListener("click", function () {
    updateHandler(user);
  });
}