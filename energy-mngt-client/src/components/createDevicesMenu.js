import { createUserDropdownItem } from "./createUserDropdown";
import { createNewDeviceCard } from "./createNewDeviceCard";

export function createDevicesFilterDropdown(users) {
  const devicesFilterContainer = document.querySelector(".devices-filter");

  const filterByLabel = document.createElement("h3");
  filterByLabel.innerText = "Devices for user";
  filterByLabel.style.color = "#305881";

  devicesFilterContainer.appendChild(filterByLabel);

  const userDropdown = createUserDropdownItem(users);
  devicesFilterContainer.appendChild(userDropdown);
}

export function createAddButton() {
  const devicesAddContainer = document.querySelector(".devices-add");

  const buttonCreate = document.createElement("button");
  buttonCreate.innerText = "Add Device";
  buttonCreate.className = "create-button";

  devicesAddContainer.appendChild(buttonCreate);

  attachEvents();
}

function createHandler() {
  const symbolHolder = document.querySelector(".edit-symbol");
  symbolHolder.classList.add("hidden");
  
  const editHolder = document.querySelector(".edit-holder");
  const editableCard = createNewDeviceCard();
  editHolder.innerHTML = "";
  editHolder.appendChild(editableCard);
  editHolder.classList.remove("hidden"); 
}

function attachEvents() {
  const createButton = document.querySelector(".create-button");
  createButton.addEventListener("click", function () {
    createHandler();
  });
}
