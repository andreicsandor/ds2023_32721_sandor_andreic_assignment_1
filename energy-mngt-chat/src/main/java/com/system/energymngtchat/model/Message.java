package com.system.energymngtchat.model;

public class Message {
    private String id;
    private String senderId;
    private String senderUsername;
    private String receiver;
    private String content;

    public Message(String id, String senderId, String senderUsername, String receiver, String content) {
        this.id = id;
        this.senderId = senderId;
        this.senderUsername = senderUsername;
        this.receiver = receiver;
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderUsername() {
        return senderUsername;
    }

    public void setSenderUsername(String senderUsername) {
        this.senderUsername = senderUsername;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
