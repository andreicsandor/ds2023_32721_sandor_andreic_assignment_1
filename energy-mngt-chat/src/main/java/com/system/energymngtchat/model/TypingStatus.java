package com.system.energymngtchat.model;

public class TypingStatus {
    private String senderId;
    private String senderUsername;
    private boolean typing;

    public TypingStatus(String senderId, String senderUsername, boolean typing) {
        this.senderId = senderId;
        this.senderUsername = senderUsername;
        this.typing = typing;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderUsername() {
        return senderUsername;
    }

    public void setSenderUsername(String senderUsername) {
        this.senderUsername = senderUsername;
    }

    public boolean getTyping() {
        return typing;
    }

    public void setTyping(boolean typing) {
        this.typing = typing;
    }
}
