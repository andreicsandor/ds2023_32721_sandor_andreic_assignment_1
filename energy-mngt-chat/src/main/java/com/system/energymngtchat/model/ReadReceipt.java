package com.system.energymngtchat.model;

public class ReadReceipt {
    private String readerId;
    private String readerUsername;

    public ReadReceipt() {
    }

    public ReadReceipt(String readerId, String readerUsername) {
        this.readerId = readerId;
        this.readerUsername = readerUsername;
    }

    public String getReaderId() {
        return readerId;
    }

    public void setReaderId(String readerId) {
        this.readerId = readerId;
    }

    public String getReaderUsername() {
        return readerUsername;
    }

    public void setReaderUsername(String readerUsername) {
        this.readerUsername = readerUsername;
    }
}
