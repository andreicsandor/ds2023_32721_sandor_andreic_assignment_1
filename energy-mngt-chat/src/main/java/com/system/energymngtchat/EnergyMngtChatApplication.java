package com.system.energymngtchat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnergyMngtChatApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnergyMngtChatApplication.class, args);
	}

}
