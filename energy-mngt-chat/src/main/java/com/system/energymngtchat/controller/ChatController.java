package com.system.energymngtchat.controller;

import com.system.energymngtchat.model.Message;
import com.system.energymngtchat.model.ReadReceipt;
import com.system.energymngtchat.model.TypingStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "*")
@Controller
public class ChatController {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @MessageMapping("/chat")
    public void sendMessage(Message message) {
        System.out.println(message);
        messagingTemplate.convertAndSend("/topic/messages", message);
    }

    @MessageMapping("/typing")
    public void sendTypingStatus(TypingStatus typingStatus) {
        System.out.println(typingStatus);
        messagingTemplate.convertAndSend("/topic/typing", typingStatus);
    }

    @MessageMapping("/receipts")
    public void sendReadReceipt(ReadReceipt readReceipt) {
        System.out.println(readReceipt);
        messagingTemplate.convertAndSend("/topic/receipts", readReceipt);
    }
}
