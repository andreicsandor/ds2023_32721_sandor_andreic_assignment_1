package com.system.energymngtusers.dto;

public class PersonDTO {
    private String personId;
    private String username;
    private String password;
    private String role;

    public PersonDTO() {
    }

    public PersonDTO(String personId, String username, String password, String role) {
        this.personId = personId;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
