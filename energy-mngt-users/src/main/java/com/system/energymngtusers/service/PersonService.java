package com.system.energymngtusers.service;

import com.system.energymngtusers.dto.NewPersonDTO;
import com.system.energymngtusers.dto.PersonDTO;
import com.system.energymngtusers.dto.PersonPatchDTO;
import jakarta.servlet.http.HttpSession;
import org.springframework.context.annotation.Bean;

import java.util.List;

public interface PersonService {
    PersonDTO createPerson(NewPersonDTO newPersonDTO);
    Boolean updatePerson(PersonPatchDTO personPatchDTO);
    Boolean deletePerson(Long id);
    List<PersonDTO> findPersons();
    List<PersonDTO> findPersonById(Long id);
    List<PersonDTO> findPersonByUsername(String username);
    List<PersonDTO> findPersonsByRole(String role);
}
