package com.system.energymngtusers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnergyMngtUsersApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnergyMngtUsersApplication.class, args);
	}

}
