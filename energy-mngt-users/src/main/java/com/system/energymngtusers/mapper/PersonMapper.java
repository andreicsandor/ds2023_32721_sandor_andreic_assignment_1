package com.system.energymngtusers.mapper;

import com.system.energymngtusers.dto.PersonDTO;
import com.system.energymngtusers.model.Person;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PersonMapper {
    public PersonDTO convertDTO(Person person) {
        PersonDTO personDTO = new PersonDTO();
        personDTO.setPersonId(String.valueOf(person.getId()));
        personDTO.setUsername(person.getUsername());
        personDTO.setPassword(person.getPassword());
        personDTO.setRole(person.getRole());

        return personDTO;
    }

    public List<PersonDTO> convertDTOs(List<Person> persons) {
        List<PersonDTO> personDTOs = new ArrayList<>();

        for (Person person : persons) {
            personDTOs.add(convertDTO(person));
        }

        return personDTOs;
    }
}
