package com.system.energymngtsimulator;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Properties;

public class EnergyMngtSimulatorApplication {

	private final static String QUEUE_NAME = "sensor";
	private static String DEVICE_ID;

	public static void main(String[] argv) throws Exception {
		// Use command 'java com.system.energymngtsimulator.EnergyMngtSimulatorApplication src/main/resources/device-7.properties'
		// to run separate instances of the simulator by changing device ID.

		if (argv.length > 0) {
			String configurationFile = argv[0];
			Properties properties = new Properties();
			try (InputStream input = new FileInputStream(configurationFile)) {
				properties.load(input);
				DEVICE_ID = properties.getProperty("device.id");
			}
		} else {
			throw new IllegalArgumentException("Configuration file not found");
		}

		String desktopPath = System.getProperty("user.home") + "/Desktop/";
		String sensorFileName = "sensor.csv";
		String sensorFilePath = desktopPath + sensorFileName;

		ConnectionFactory factory = new ConnectionFactory();
		factory.setUri("amqps://fskyrapo:PvM7SG6cVbae5Atn2pZvPtMP6a0EA9lF@sparrow.rmq.cloudamqp.com/fskyrapo");

		try (Connection connection = factory.newConnection(); Channel channel = connection.createChannel()) {
			channel.queueDeclare(QUEUE_NAME, true, false, false, null);

			try (BufferedReader reader = new BufferedReader(new FileReader(sensorFilePath))) {
				String measurement;
				while ((measurement = reader.readLine()) != null) {
					String json = createMessageJSON(measurement);
					channel.basicPublish("", QUEUE_NAME, null, json.getBytes(StandardCharsets.UTF_8));
					System.out.println("Measurement read from sensor " + measurement);
					System.out.println("Sent measurement message!");
					Thread.sleep(6000);
				}
			}
		}
	}

	private static String createMessageJSON(String measurement) {
		String type = "measurement";
		double measurementValue = Double.parseDouble(measurement.trim());
		String measurementJSON = createMeasurementJSON(DEVICE_ID, measurementValue);

		return String.format(
				"{\"type\": \"%s\", \"body\": %s}",
				type,
				measurementJSON
		);
	}

	private static String createMeasurementJSON(String deviceId, double measurementValue) {
		long timestamp = new Date().getTime();
		return String.format(
				"{\"timestamp\": %d, \"deviceId\": \"%s\", \"measurementValue\": %.1f}",
				timestamp,
				deviceId,
				measurementValue
		);
	}
}
