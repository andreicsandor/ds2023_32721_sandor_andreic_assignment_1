package com.system.energymngtconsumer.services.impl;

import com.system.energymngtconsumer.dto.MeasurementAggregateDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.system.energymngtconsumer.entities.Measurement;
import com.system.energymngtconsumer.repositories.MeasurementDAO;
import com.system.energymngtconsumer.services.MeasurementService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class MeasurementServiceImpl implements MeasurementService {
    @Autowired
    private MeasurementDAO measurementDAO;
    @Autowired
    @Qualifier("restTemplateConsumer")
    private RestTemplate restTemplate;
    private final String devicesServiceUrl = "http://localhost:8080";

    @Override
    public Measurement createMeasurement(Measurement measurement) {
        return measurementDAO.save(measurement);
    }

    @Override
    public Optional<Measurement> findLastMeasurementForDeviceWithinHour(Long deviceId, Date timestamp) {
        Calendar time = Calendar.getInstance();
        time.setTime(timestamp);
        time.set(Calendar.MINUTE, 0);
        time.set(Calendar.SECOND, 0);
        time.set(Calendar.MILLISECOND, 0);
        Date startOfHour = time.getTime();
        time.add(Calendar.HOUR_OF_DAY, 1);
        Date endOfHour = time.getTime();

        return measurementDAO.findTopByDeviceIdAndTimestampBetweenOrderByTimestampDesc(deviceId, startOfHour, endOfHour);
    }

    @Override
    public Optional<Double> getDeviceThreshold(Long deviceId) {
        String url = devicesServiceUrl + "/devices/" + deviceId + "/consumption";
        try {
            // Gets the object & deserializes into a Double object
            Double consumption = restTemplate.getForObject(url, Double.class);
            return Optional.ofNullable(consumption);
        } catch (Exception e) {
            System.out.println("Error retrieving consumption for device ID: " + deviceId);
            return Optional.empty();
        }
    }

    @Override
    public Long getUserIdByDeviceId(Long deviceId) {
        final String url = devicesServiceUrl + "/devices/" + deviceId + "/owner";
        try {
            // Gets the object & deserializes into a Long object
            Long personId = restTemplate.getForObject(url, Long.class);
            return personId;
        } catch (Exception e) {
            System.out.println("Error retrieving user for device ID: " + deviceId);
            return null;
        }
    }

    @Override
    public List<MeasurementAggregateDTO> getMeasurementsForDeviceByDate(Long deviceId, String date) throws ParseException {
        Date formattedDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);

        Calendar startCal = Calendar.getInstance();
        startCal.setTime(formattedDate);
        startCal.set(Calendar.HOUR_OF_DAY, 0);
        startCal.set(Calendar.MINUTE, 0);
        startCal.set(Calendar.SECOND, 0);
        startCal.set(Calendar.MILLISECOND, 0);

        Calendar endCal = (Calendar) startCal.clone();
        endCal.add(Calendar.DAY_OF_MONTH, 1);

        Date startDate = startCal.getTime();
        Date endDate = endCal.getTime();

        List<Measurement> measurements = measurementDAO.findByDeviceIdAndDay(deviceId, startDate, endDate);

        Map<Integer, Double> hourlyConsumptionMap = new TreeMap<>();
        for (Measurement measurement : measurements) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(measurement.getTimestamp());
            int hour = calendar.get(Calendar.HOUR_OF_DAY);

            hourlyConsumptionMap.merge(hour, measurement.getMeasurementValue(), Double::sum);
        }

        List<MeasurementAggregateDTO> aggregates = new ArrayList<>();
        for(Map.Entry<Integer, Double> entry : hourlyConsumptionMap.entrySet()) {
            aggregates.add(new MeasurementAggregateDTO(entry.getKey(), entry.getValue()));
        }

        return aggregates;
    }
}
