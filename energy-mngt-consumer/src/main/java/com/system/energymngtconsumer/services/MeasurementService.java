package com.system.energymngtconsumer.services;

import com.system.energymngtconsumer.dto.MeasurementAggregateDTO;
import com.system.energymngtconsumer.entities.Measurement;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface MeasurementService {
    Measurement createMeasurement(Measurement measurement);
    Optional<Measurement> findLastMeasurementForDeviceWithinHour(Long deviceId, Date timestamp);
    Optional<Double> getDeviceThreshold(Long device_id);
    Long getUserIdByDeviceId(Long deviceId);
    List<MeasurementAggregateDTO> getMeasurementsForDeviceByDate(Long deviceId, String dateStr) throws ParseException;
}
