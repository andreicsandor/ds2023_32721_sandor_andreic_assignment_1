package com.system.energymngtconsumer.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Configuration
@Component("consumerAppConfig")
public class AppConfig {

    @Bean(name = "restTemplateConsumer")
    public RestTemplate restTemplateConsumer() {
        return new RestTemplate();
    }
}
