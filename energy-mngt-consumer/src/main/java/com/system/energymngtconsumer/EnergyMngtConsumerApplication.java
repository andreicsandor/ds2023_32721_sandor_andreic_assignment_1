package com.system.energymngtconsumer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.system.energymngtconsumer.entities.Notification;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import com.system.energymngtconsumer.entities.Measurement;
import com.system.energymngtconsumer.services.MeasurementService;
import com.system.energymngtconsumer.websocket.WebSocketController;

import java.util.Optional;

@SpringBootApplication
public class EnergyMngtConsumerApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(EnergyMngtConsumerApplication.class, args);
	}

	@Autowired
	private WebSocketController webSocketController;

	@Autowired
	private MeasurementService measurementService;

	@RabbitListener(queues = "sensor")
	public void receiveMessage(String message) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			JsonNode rootNode = objectMapper.readTree(message);
			JsonNode messageTypeNode = rootNode.get("type");
			JsonNode messageBodyNode = rootNode.get("body");
			String messageType = messageTypeNode.asText();

			switch (messageType) {
				case "measurement":
					Measurement measurementMessage = objectMapper.treeToValue(messageBodyNode, Measurement.class);
					processMeasurementMessage(measurementMessage);
					break;
				case "notification":
					Notification notificationMessage = objectMapper.treeToValue(messageBodyNode, Notification.class);
					processNotificationMessage(notificationMessage);
					break;
				default:
					System.out.println("Unknown message type: " + messageType);
			}
		} catch (Exception e) {
			System.out.println("Error processing message");
		}
	}

	private void processMeasurementMessage(Measurement measurement) {
		Optional<Double> maximumCapacityOptional = measurementService.getDeviceThreshold(measurement.getDeviceId());

		if (maximumCapacityOptional.isPresent()) {
			double maximumCapacity = maximumCapacityOptional.get();

			Optional<Measurement> previousMeasurementOptional = measurementService.findLastMeasurementForDeviceWithinHour(
				measurement.getDeviceId(),
				measurement.getTimestamp()
			);

			double totalConsumption;

			if (previousMeasurementOptional.isPresent()) {
				Measurement previousMeasurement = previousMeasurementOptional.get();
				totalConsumption = previousMeasurement.getTotalConsumption() + measurement.getMeasurementValue();
			} else {
				totalConsumption = measurement.getMeasurementValue();
			}

			measurement.setTotalConsumption(totalConsumption);

			if (totalConsumption > maximumCapacity) {
				Long deviceId = measurement.getDeviceId();
				String alertMessage = "Overload for device " + deviceId + ".";

				Long userId = measurementService.getUserIdByDeviceId(measurement.getDeviceId());
				if (userId != null) {
					webSocketController.sendMessageToUser(String.valueOf(userId), alertMessage);
					System.out.println("Sent overload message after new record!");
				}
			}

			measurementService.createMeasurement(measurement);
			System.out.println("Measurement saved.");
		} else {
			System.out.println("No device found for ID: " + measurement.getDeviceId());
		}
	}

	private void processNotificationMessage(Notification notification) {
		Optional<Double> maximumCapacityOptional = measurementService.getDeviceThreshold(notification.getDeviceId());

		if (maximumCapacityOptional.isPresent()) {
			double maximumCapacity = maximumCapacityOptional.get();

			Optional<Measurement> previousMeasurementOptional = measurementService.findLastMeasurementForDeviceWithinHour(
				notification.getDeviceId(),
				notification.getTimestamp()
			);

			double totalConsumption;

			if (previousMeasurementOptional.isPresent()) {
				Measurement previousMeasurement = previousMeasurementOptional.get();
				totalConsumption = previousMeasurement.getTotalConsumption();
			} else {
				totalConsumption = 0;
			}

			if (totalConsumption > maximumCapacity) {
				Long deviceId = notification.getDeviceId();
				String alertMessage = "Overload for device " + deviceId + ".";

				Long userId = measurementService.getUserIdByDeviceId(deviceId);
				if (userId != null) {
					webSocketController.sendMessageToUser(String.valueOf(userId), alertMessage);
					System.out.println("Sent overload message after threshold update!");
				}
			}
		} else {
			System.out.println("No device found for ID: " + notification.getDeviceId());
		}
	}
}
