package com.system.energymngtconsumer.repositories;

import com.system.energymngtconsumer.dto.MeasurementAggregateDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.system.energymngtconsumer.entities.Measurement;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface MeasurementDAO extends JpaRepository<Measurement, Long> {
    List<Measurement> findByDeviceIdAndTimestampBetweenOrderByTimestampDesc(int deviceId, Date start, Date end);
    Optional<Measurement> findTopByDeviceIdOrderByTimestampDesc(Long deviceId);
    Optional<Measurement> findTopByDeviceIdAndTimestampBetweenOrderByTimestampDesc(Long deviceId, Date startOfHour, Date endOfHour);
    @Query("SELECT m FROM Measurement m WHERE m.deviceId = :deviceId AND m.timestamp >= :startOfDay AND m.timestamp < :endOfDay")
    List<Measurement> findByDeviceIdAndDay(Long deviceId, Date startOfDay, Date endOfDay);
}

