package com.system.energymngtconsumer.controllers;

import com.system.energymngtconsumer.dto.MeasurementAggregateDTO;
import com.system.energymngtconsumer.services.MeasurementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@CrossOrigin(origins = "*")
@Controller
public class MeasurementController {

    @Autowired
    private MeasurementService measurementService;
    @GetMapping("/measurements")
    public ResponseEntity<?> getMeasurementsForDeviceByDate(
            @RequestParam Long deviceId,
            @RequestParam String date) throws ParseException {
        List<MeasurementAggregateDTO> measurementAggregates = measurementService.getMeasurementsForDeviceByDate(deviceId, date);

        if (measurementAggregates != null) {
            return ResponseEntity.ok(measurementAggregates);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

}