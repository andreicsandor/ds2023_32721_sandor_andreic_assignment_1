package com.system.energymngtconsumer.dto;

public class MeasurementAggregateDTO {
    private int hour;
    private double hourlyConsumption;

    public MeasurementAggregateDTO() {

    }

    public MeasurementAggregateDTO(int hour, double hourlyConsumption) {
        this.hour = hour;
        this.hourlyConsumption = hourlyConsumption;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public double getHourlyConsumption() {
        return hourlyConsumption;
    }

    public void setHourlyConsumption(double hourlyConsumption) {
        this.hourlyConsumption = hourlyConsumption;
    }
}
