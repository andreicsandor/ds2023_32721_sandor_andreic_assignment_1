package com.system.energymngtconsumer.entities;

public abstract class Message {
    private String type;

    public Message() {
    }

    public Message(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

