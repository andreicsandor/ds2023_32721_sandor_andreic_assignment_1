package com.system.energymngtconsumer.entities;

import jakarta.persistence.*;
import java.util.Date;

@Entity
public class Measurement extends Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "device_id", nullable = false)
    private Long deviceId;

    @Column(name = "timestamp", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @Column(name = "measurementValue", nullable = false)
    private Double measurementValue;

    @Column(name = "totalConsumption",nullable = true)
    private Double totalConsumption;

    public Measurement() {
        super();
    }

    public Measurement(String type, Long deviceId, Date timestamp, Double measurement_value, Double total_consumption) {
        super.setType(type);
        this.deviceId = deviceId;
        this.timestamp = timestamp;
        this.measurementValue = measurement_value;
        this.totalConsumption = total_consumption;
    }

    public Long getId() {
        return id;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Double getMeasurementValue() {
        return measurementValue;
    }

    public void setMeasurementValue(Double measurementValue) {
        this.measurementValue = measurementValue;
    }

    public Double getTotalConsumption() {
        return totalConsumption;
    }

    public void setTotalConsumption(Double totalConsumption) {
        this.totalConsumption = totalConsumption;
    }
}
