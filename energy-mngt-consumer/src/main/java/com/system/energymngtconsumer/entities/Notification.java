package com.system.energymngtconsumer.entities;

import java.util.Date;

public class Notification extends Message {
    private Long deviceId;
    private Date timestamp;

    public Notification() {
        super();
    }

    public Notification(String type, Date timestamp, String message) {
        super.setType(type);
        this.timestamp = timestamp;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
