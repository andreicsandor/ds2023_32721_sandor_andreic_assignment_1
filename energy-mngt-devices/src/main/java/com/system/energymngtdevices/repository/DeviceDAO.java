package com.system.energymngtdevices.repository;

import com.system.energymngtdevices.model.Device;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface DeviceDAO extends CrudRepository<Device, Long> {
    List<Device> findAll();
    Optional<Device> findById(Long id);
    List<Device> findByAddress(String address);
    List<Device> findByConsumption(Double consumption);
    List<Device> findByPersonId(Long personId);
}
