package com.system.energymngtdevices.controller;

import com.system.energymngtdevices.dto.NewDeviceDTO;
import com.system.energymngtdevices.dto.DeviceDTO;
import com.system.energymngtdevices.dto.DevicePatchDTO;
import com.system.energymngtdevices.service.DeviceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*")
@RestController
public class DeviceController {
    private DeviceService deviceService;

    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @GetMapping("/devices")
    public List<DeviceDTO> getDevices(@RequestParam(value = "deviceId", required = false) Long deviceId,
                                      @RequestParam(value = "address", required = false) String address,
                                      @RequestParam(value = "consumption", required = false) String consumption,
                                      @RequestParam(value = "personId", required = false) Long personId) {
        List<DeviceDTO> devices;

        if (deviceId != null) {
            devices = deviceService.findDeviceById(deviceId);
        } else if (address != null) {
            devices = deviceService.findDeviceByAddress(address);
        } else if (consumption != null) {
            Double formattedConsumption = Double.parseDouble(consumption);
            devices = deviceService.findDevicesByConsumption(formattedConsumption);
        } else if (personId != null) {
            devices = deviceService.findDevicesByPersonId(personId);
        } else {
            devices = deviceService.findDevices();
        }

        if (devices.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No devices found");
        }

        return devices;
    }

    @PostMapping("/create-device")
    public ResponseEntity<?> createDevice(@RequestBody NewDeviceDTO newDeviceDTO) {
        DeviceDTO deviceDTO = deviceService.createDevice(newDeviceDTO);

        if (deviceDTO != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(deviceDTO);
        } else {
            Map<String, String> response = new HashMap<>();
            response.put("message", "Failed to create device.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
    }

//    @PatchMapping("/update-device")
//    public ResponseEntity<?> updateDevice(@RequestBody DevicePatchDTO devicePatchDTO) {
//        Boolean result = deviceService.updateDevice(devicePatchDTO);
//
//        if (result != false) {
//            Map<String, String> response = new HashMap<>();
//            response.put("message", "Device updated successfully.");
//            return ResponseEntity.status(HttpStatus.OK).body(response);
//        } else {
//            Map<String, String> response = new HashMap<>();
//            response.put("message", "Failed to update device.");
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
//        }
//    }

    @PutMapping("/update-device")
    public ResponseEntity<?> updatePerson(@RequestBody DevicePatchDTO devicePatchDTO) {
        Boolean result = deviceService.updateDevice(devicePatchDTO);

        if (result != false) {
            Map<String, String> response = new HashMap<>();
            response.put("message", "Device updated successfully.");
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } else {
            Map<String, String> response = new HashMap<>();
            response.put("message", "Failed to update device.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
    }

    @DeleteMapping("/delete-device/{id}")
    public ResponseEntity<?> deleteDevice(@PathVariable Long id) {
        Boolean result = deviceService.deleteDevice(id);

        if (result != false) {
            Map<String, String> response = new HashMap<>();
            response.put("message", "Device deleted successfully.");
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } else {
            Map<String, String> response = new HashMap<>();
            response.put("message", "Failed to delete Device.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
    }

    @DeleteMapping("/delete-devices/person/{personId}")
    public ResponseEntity<?> deleteDevicesByPersonId(@PathVariable Long personId) {
        Boolean result = deviceService.deleteDevicesByPersonId(personId);
        System.out.println(personId);

        if (result != false) {
            Map<String, String> response = new HashMap<>();
            response.put("message", "Devices deleted successfully.");
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } else {
            Map<String, String> response = new HashMap<>();
            response.put("message", "Failed to delete devices.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
    }

    @GetMapping("/devices/{id}/consumption")
    public ResponseEntity<Double> getDeviceConsumptionById(@PathVariable Long id) {
        Double consumption = deviceService.getDeviceConsumptionById(id);
        if (consumption != null) {
            return ResponseEntity.ok(consumption);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping("/devices/{id}/owner")
    public ResponseEntity<Long> getDeviceOwner(@PathVariable Long id) {
        Long personId = deviceService.getUserIdByDeviceId(id);
        if (personId != null) {
            return ResponseEntity.ok(personId);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}