package com.system.energymngtdevices.mapper;

import com.system.energymngtdevices.dto.DeviceDTO;
import com.system.energymngtdevices.model.Device;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DeviceMapper {
    public DeviceDTO convertDTO(Device device) {
        DeviceDTO deviceDTO = new DeviceDTO();
        deviceDTO.setDeviceId(String.valueOf(device.getId()));
        deviceDTO.setName(device.getName());
        deviceDTO.setDescription(device.getDescription());
        deviceDTO.setAddress(device.getAddress());
        deviceDTO.setConsumption(String.valueOf(device.getConsumption()));
        deviceDTO.setPersonId(String.valueOf(device.getPersonId()));

        return deviceDTO;
    }

    public List<DeviceDTO> convertDTOs(List<Device> devices) {
        List<DeviceDTO> deviceDTOs = new ArrayList<>();

        for (Device device : devices) {
            deviceDTOs.add(convertDTO(device));
        }

        return deviceDTOs;
    }
}
