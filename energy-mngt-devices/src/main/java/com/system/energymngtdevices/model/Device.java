package com.system.energymngtdevices.model;

import jakarta.persistence.*;

@Entity
@Table(name="Device", schema = "dbo")
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "device_id")
    private Long id;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "description", nullable = false, length = 100)
    private String description;

    @Column(name = "address", nullable = false, length = 45)
    private String address;

    @Column(name = "consumption", nullable = false, length = 50)
    private Double consumption;

    @Column(name = "personId", nullable = false, length = 50)
    private Long personId;

    public Device() {
    }

    public Device(Long id, String name, String description, String address, Double consumption, Long personId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.address = address;
        this.consumption = consumption;
        this.personId = personId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getConsumption() {
        return consumption;
    }

    public void setConsumption(Double consumption) {
        this.consumption = consumption;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }
}
