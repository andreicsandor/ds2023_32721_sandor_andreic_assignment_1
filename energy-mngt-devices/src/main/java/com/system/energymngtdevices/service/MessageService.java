package com.system.energymngtdevices.service;

public interface MessageService {
    String createMessageJSON(Long deviceId);
    String createNotificationJSON(Long deviceId);
}
