package com.system.energymngtdevices.service.impl;

import com.system.energymngtdevices.service.MessageService;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MessageServiceImpl implements MessageService {
    @Override
    public String createMessageJSON(Long deviceId) {
        String type = "notification";
        String notificationJSON = createNotificationJSON(deviceId);

        return String.format(
                "{\"type\": \"%s\", \"body\": %s}",
                type,
                notificationJSON
        );
    }

    @Override
    public String createNotificationJSON(Long deviceId) {
        long timestamp = new Date().getTime();
        return String.format(
                "{\"deviceId\": \"%d\", \"timestamp\": %d}",
                deviceId,
                timestamp
        );
    }

}
