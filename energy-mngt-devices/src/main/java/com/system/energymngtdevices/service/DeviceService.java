package com.system.energymngtdevices.service;

import com.system.energymngtdevices.dto.NewDeviceDTO;
import com.system.energymngtdevices.dto.DeviceDTO;
import com.system.energymngtdevices.dto.DevicePatchDTO;

import java.util.List;

public interface DeviceService {
    DeviceDTO createDevice(NewDeviceDTO newDeviceDTO);
    Boolean updateDevice(DevicePatchDTO DevicePatchDTO);
    Boolean deleteDevice(Long id);
    Boolean deleteDevicesByPersonId(Long personId);
    List<DeviceDTO> findDevices();
    List<DeviceDTO> findDeviceById(Long id);
    List<DeviceDTO> findDeviceByAddress(String address);
    List<DeviceDTO> findDevicesByConsumption(Double consumption);
    List<DeviceDTO> findDevicesByPersonId(Long personId);
    Double getDeviceConsumptionById(Long id);
    Long getUserIdByDeviceId(Long deviceId);
}
