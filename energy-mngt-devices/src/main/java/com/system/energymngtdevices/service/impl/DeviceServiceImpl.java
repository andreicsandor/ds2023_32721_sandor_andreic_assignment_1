package com.system.energymngtdevices.service.impl;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.system.energymngtdevices.dto.NewDeviceDTO;
import com.system.energymngtdevices.dto.DeviceDTO;
import com.system.energymngtdevices.dto.DevicePatchDTO;
import com.system.energymngtdevices.mapper.DeviceMapper;
import com.system.energymngtdevices.model.Device;
import com.system.energymngtdevices.repository.DeviceDAO;
import com.system.energymngtdevices.service.DeviceService;
import com.system.energymngtdevices.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.util.*;

@Service
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    private DeviceDAO deviceDAO;
    @Autowired
    private MessageService messageService;
    @Autowired
    private DeviceMapper deviceMapper;

    private final static String QUEUE_NAME = "sensor";

    @Override
    public DeviceDTO createDevice(NewDeviceDTO newDeviceDTO) {
        String name = newDeviceDTO.getName();
        String description = newDeviceDTO.getDescription();
        String address = newDeviceDTO.getAddress();
        Double consumption = Double.valueOf(newDeviceDTO.getConsumption());
        Long personId = Long.valueOf(newDeviceDTO.getPersonId());

        Device device = new Device();
        device.setName(name);
        device.setDescription(description);
        device.setAddress(address);
        device.setConsumption(consumption);
        device.setPersonId(personId);

        device = deviceDAO.save(device);

        if(device == null) {
            return null;
        }

        DeviceDTO deviceDTO = deviceMapper.convertDTO(device);
        return deviceDTO;
    }

    @Override
    public Boolean updateDevice(DevicePatchDTO devicePatchDTO) {
        Long deviceId = Long.valueOf(devicePatchDTO.getId());
        String name = devicePatchDTO.getName();
        String description = devicePatchDTO.getDescription();
        String address = devicePatchDTO.getAddress();
        Double consumption = Double.valueOf(devicePatchDTO.getConsumption());
        Long personId = Long.valueOf(devicePatchDTO.getPersonId());

        Optional<Device> deviceEntity = deviceDAO.findById(deviceId);

        if (!deviceEntity.isPresent()) {
            return false;
        }

        Device device = deviceEntity.get();

        ConnectionFactory factory = new ConnectionFactory();
        try {
            factory.setUri("amqps://fskyrapo:PvM7SG6cVbae5Atn2pZvPtMP6a0EA9lF@sparrow.rmq.cloudamqp.com/fskyrapo");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        if (!consumption.equals(device.getConsumption())) {
            String jsonMessage = messageService.createMessageJSON(deviceId);

            try (Connection connection = factory.newConnection(); Channel channel = connection.createChannel()) {
                channel.queueDeclare(QUEUE_NAME, true, false, false, null);
                channel.basicPublish("", QUEUE_NAME, null, jsonMessage.getBytes(StandardCharsets.UTF_8));
                System.out.println("Updated threshold to " + consumption + " for device ID " + deviceId);
                System.out.println("Sent notification message!");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        device.setName(name);
        device.setDescription(description);
        device.setAddress(address);
        device.setConsumption(consumption);
        device.setPersonId(personId);

        deviceDAO.save(device);

        return true;
    }

    @Override
    public Boolean deleteDevice(Long id) {
        Optional<Device> deviceEntity = deviceDAO.findById(id);

        if (!deviceEntity.isPresent()) {
            return false;
        }

        Device device = deviceEntity.get();
        deviceDAO.delete(device);

        return true;
    }

    @Override
    @Transactional
    public Boolean deleteDevicesByPersonId(Long personId) {
        List<Device> devices = deviceDAO.findByPersonId(personId);

        if (devices.isEmpty()) {
            return true;
        }

        deviceDAO.deleteAll(devices);

        return true;
    }

    @Override
    public List<DeviceDTO> findDevices() {
        List<Device> devices = deviceDAO.findAll();
        return deviceMapper.convertDTOs(devices);
    }

    @Override
    public List<DeviceDTO> findDeviceById(Long id) {
        Optional<Device> deviceEntity = deviceDAO.findById(id);

        if (!deviceEntity.isPresent()) {
            return Collections.emptyList();
        }

        Device device = deviceEntity.get();
        List<DeviceDTO> devicesDTOs = new ArrayList<>();
        devicesDTOs.add(deviceMapper.convertDTO(device));

        return devicesDTOs;
    }

    @Override
    public List<DeviceDTO> findDeviceByAddress(String address) {
        List<Device> devices = deviceDAO.findByAddress(address);
        return deviceMapper.convertDTOs(devices);
    }

    @Override
    public List<DeviceDTO> findDevicesByConsumption(Double consumption) {
        List<Device> devices = deviceDAO.findByConsumption(consumption);
        return deviceMapper.convertDTOs(devices);
    }

    @Override
    public List<DeviceDTO> findDevicesByPersonId(Long personId) {
        List<Device> devices = deviceDAO.findByPersonId(personId);
        return deviceMapper.convertDTOs(devices);
    }

    @Override
    public Double getDeviceConsumptionById(Long id) {
        Optional<Device> deviceEntity = deviceDAO.findById(id);
        if (deviceEntity.isPresent()) {
            return deviceEntity.get().getConsumption();
        } else {
            return null;
        }
    }

    public Long getUserIdByDeviceId(Long id) {
        Optional<Device> deviceEntity = deviceDAO.findById(id);
        if (deviceEntity.isPresent()) {
            return deviceEntity.get().getPersonId();
        } else {
            return null;
        }
    }
}
