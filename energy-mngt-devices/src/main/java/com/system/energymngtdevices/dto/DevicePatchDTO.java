package com.system.energymngtdevices.dto;

public class DevicePatchDTO {
    private String id;
    private String name;
    private String description;
    private String address;
    private String consumption;
    private String personId;

    public DevicePatchDTO() {
    }

    public DevicePatchDTO(String id, String name, String description, String address, String consumption, String personId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.address = address;
        this.consumption = consumption;
        this.personId = personId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getConsumption() {
        return consumption;
    }

    public void setConsumption(String consumption) {
        this.consumption = consumption;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }
}
