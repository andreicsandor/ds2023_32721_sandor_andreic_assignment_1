package com.system.energymngtdevices.dto;

public class DeviceDTO {
    private String deviceId;
    private String name;
    private String description;
    private String address;
    private String consumption;
    private String personId;

    public DeviceDTO() {
    }

    public DeviceDTO(String deviceId, String name, String description, String address, String consumption, String personId) {
        this.deviceId = deviceId;
        this.name = name;
        this.description = description;
        this.address = address;
        this.consumption = consumption;
        this.personId = personId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getConsumption() {
        return consumption;
    }

    public void setConsumption(String consumption) {
        this.consumption = consumption;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }
}
