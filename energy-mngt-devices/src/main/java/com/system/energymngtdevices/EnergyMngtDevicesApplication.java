package com.system.energymngtdevices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnergyMngtDevicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnergyMngtDevicesApplication.class, args);
	}

}
